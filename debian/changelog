bucardo (5.6.0-5) UNRELEASED; urgency=medium

  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on perl.
    + bucardo: Drop versioned constraint on lsb-base and perl in Depends.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 11 Oct 2021 19:02:50 -0000

bucardo (5.6.0-4) unstable; urgency=medium

  * Source only upload

 -- Michael Meskes <meskes@debian.org>  Wed, 30 Jun 2021 07:48:57 +0200

bucardo (5.6.0-3) unstable; urgency=medium

  [ Hideki Yamane ]
  * Fix upgrade failure with perl-modules-5.24 from stretch is still installed
    (Closes: #988358)

 -- Michael Meskes <meskes@debian.org>  Mon, 28 Jun 2021 11:24:33 +0200

bucardo (5.6.0-2) unstable; urgency=medium

  * Team upload.
  * autopkgtest: Start service directly as "service" is unreliable in CI
    environments.

 -- Christoph Berg <myon@debian.org>  Mon, 23 Nov 2020 14:57:20 +0100

bucardo (5.6.0-1) unstable; urgency=medium

  * Team upload.

  [ Christoph Berg ]
  * New upstream version 5.6.0.
  * Add debian/gitlab-ci.yml.
  * Use source format 3.0 (quilt).
  * Always start daemon by default. /etc/default/bucardo is obsolete now.
  * debian/tests: Test starting the server.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Name, Repository.
  * debian/watch: Fix for download via https.

 -- Christoph Berg <myon@debian.org>  Wed, 18 Nov 2020 14:25:53 +0100

bucardo (5.5.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add dependency on libpod-parser-perl (Closes: #961208)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 08 Nov 2020 18:26:12 +0000

bucardo (5.5.0-1) unstable; urgency=medium

  [ Christoph Berg ]
  * Move maintainer address to team+postgresql@tracker.debian.org.

  [ Michael Meskes ]
  * New upstream version 5.5.0
  * Make sure bucardo uses /usr/bin/perl directly.

 -- Michael Meskes <meskes@debian.org>  Tue, 13 Nov 2018 13:35:13 +0100

bucardo (5.4.1-2) unstable; urgency=medium

  * Updated watch file.
  * Bumped Standards-Version to 4.1.3, no changes needed.
  * Added lsb-base dependency.

 -- Michael Meskes <meskes@debian.org>  Mon, 01 Jan 2018 11:01:09 +0100

bucardo (5.4.1-1) unstable; urgency=medium

  * Imported Upstream version 5.4.1
  * Bumped debhelper version, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Mon, 11 Jan 2016 14:59:47 +0100

bucardo (5.4.0-1) unstable; urgency=medium

  * Imported Upstream version 5.4.0
  * Added patch from git to place sequence into correct schema.

 -- Michael Meskes <meskes@debian.org>  Sun, 30 Aug 2015 14:36:44 +0200

bucardo (5.3.1-1) unstable; urgency=medium

  * Imported Upstream version 5.3.1

 -- Michael Meskes <meskes@debian.org>  Thu, 18 Jun 2015 11:34:09 +0200

bucardo (5.1.2-1) unstable; urgency=medium

  * Imported Upstream version 5.1.2
  * Bumped Standards-Version to 3.9.6, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Mon, 20 Oct 2014 11:35:09 +0200

bucardo (5.1.1-1) unstable; urgency=medium

  * Imported Upstream version 5.1.1

 -- Michael Meskes <meskes@debian.org>  Mon, 11 Aug 2014 12:21:06 +0200

bucardo (5.1.0-1) unstable; urgency=medium

  * Imported Upstream version 5.1.0

 -- Michael Meskes <meskes@debian.org>  Wed, 16 Jul 2014 10:48:07 +0200

bucardo (5.0.0-1) unstable; urgency=medium

  * Imported Upstream version 5.0.0
  * Bumped Standards-Version to 3.9.5, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sun, 06 Jul 2014 15:17:52 +0200

bucardo (4.99.10-1) unstable; urgency=low

  * Imported Upstream version 4.99.10

 -- Michael Meskes <meskes@debian.org>  Fri, 22 Nov 2013 16:02:27 +0100

bucardo (4.99.8-1) unstable; urgency=low

  * Imported Upstream version 4.99.8
  * Made init script report correct result. (Closes: #724878)
  * Canonicalized VCS fields.

 -- Michael Meskes <meskes@debian.org>  Wed, 06 Nov 2013 15:40:17 +0100

bucardo (4.99.7-1) unstable; urgency=low

  * Imported Upstream version 4.99.7
  * Bumped Standards-Version to 3.9.4, no changes needed.

 -- Michael Meskes <meskes@debian.org>  Sun, 24 Feb 2013 10:57:08 +0100

bucardo (4.99.6-1) unstable; urgency=low

  [ Christoph Berg ]
  * Change recommends to postgresql-plperl without an explicit postgresql
    version.

  [ Michael Meskes ]
  * Imported Upstream version 4.99.6

 -- Michael Meskes <meskes@debian.org>  Tue, 08 Jan 2013 13:19:39 +0100

bucardo (4.99.5-1) unstable; urgency=low

  * Imported Upstream version 4.99.5
  * Added VCS information
  * Do not ship a bucardo directory in /var/run. It gets created by the init
    script anyway.
  * Removed recommendation of postgresql-plperl-9.0.

 -- Michael Meskes <meskes@debian.org>  Mon, 04 Jun 2012 11:44:00 +0200

bucardo (4.99.3-1) unstable; urgency=low

  * Imported Upstream version 4.99.3
  * Bumped Standards-Version to 3.9.3, no changes needed.
  * Do not try to upgrade bucardo when coming from version 4.
  * Package now removes homedir (if empty) and locks user account on purge.
    (Closes: #656465)
  * Added 'status' option to init file.
  * Added dependency on libboolean-perl.

 -- Michael Meskes <meskes@debian.org>  Mon, 05 Mar 2012 15:28:36 +0100

bucardo (4.4.8-1) unstable; urgency=low

  * Imported Upstream version 4.4.8

 -- Michael Meskes <meskes@debian.org>  Fri, 23 Dec 2011 11:40:26 +0100

bucardo (4.4.6-1) unstable; urgency=low

  * Also support PostgreSQL 9.1 as dependency.
  * Imported Upstream version 4.4.6
  * Converted patch system to quilt.

 -- Michael Meskes <meskes@debian.org>  Thu, 25 Aug 2011 13:08:22 +0200

bucardo (4.4.5-1) unstable; urgency=low

  [ Nicholas Jefferson ]
  * New version of Bucardo

  [ Michael Meskes ]
  * Bumped Standards-Version to 3.9.2, no changes needed.
  * Added source/format file.

 -- Michael Meskes <meskes@debian.org>  Wed, 06 Jul 2011 09:46:56 +0200

bucardo (4.4.4-1) unstable; urgency=low

  * New version of Bucardo
  * Added patch to set session authorization in bucardo.schema script (Closes: #630785)
  * Improved instructions in README.Debian

 -- Nicholas Jefferson <nicholas@pythonic.com.au>  Tue, 21 Jun 2011 11:58:50 +1000

bucardo (4.4.3-1) unstable; urgency=low

  * New version of Bucardo

 -- Nicholas Jefferson <nicholas@pythonic.com.au>  Sun, 17 Apr 2011 18:24:41 +1000

bucardo (4.4.2-1) unstable; urgency=low

  * New version of Bucardo

 -- Nicholas Jefferson <nicholas@pythonic.com.au>  Sat, 26 Mar 2011 18:03:32 +1100

bucardo (4.4.0-1) unstable; urgency=low

  * Initial release (Closes: #497650)

 -- Nicholas Jefferson <nicholas@pythonic.com.au>  Sat, 26 Sep 2009 17:36:16 +1000
